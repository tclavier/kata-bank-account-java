package fr.univlille.iut.info.r304.ds;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CompteEnBanque {
    private static final List<String> nomsDesComptes = new ArrayList<>();
    private final List<Transaction> transactions = new ArrayList<>();
    private double valeurDuCompte = 0;

    public static CompteEnBanque creer(String nom) {
        if (nomsDesComptes.contains(nom)) throw new RuntimeException("Le compte existe déjà");
        CompteEnBanque compteEnBanque = new CompteEnBanque();
        compteEnBanque.transactions.add(Transaction.of("Création"));
        nomsDesComptes.add(nom);
        return compteEnBanque;
    }

    public static void nettoyer() {
        nomsDesComptes.clear();
    }

    public CompteEnBanque with(double montant) {
        valeurDuCompte = montant;
        this.transactions.add(Transaction.of(String.format(Locale.US, "Dépôt de %.2f €", montant)));
        return this;
    }

    public double solde() {
        return valeurDuCompte;
    }

    public void depot(double montant) {
        this.valeurDuCompte += montant;
        this.transactions.add(Transaction.of(String.format(Locale.US, "Dépôt de %.2f €", montant)));
    }

    public void retrait(double montant) {
        if (this.valeurDuCompte - montant < 0) throw new RuntimeException("Il manque de l'argent");
        this.transactions.add(Transaction.of(String.format(Locale.US, "Retrait de %.2f €", montant)));
        this.valeurDuCompte -= montant;
    }

    public List<Transaction> historique() {
        return transactions;
    }
}