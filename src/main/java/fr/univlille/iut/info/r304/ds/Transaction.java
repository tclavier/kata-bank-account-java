package fr.univlille.iut.info.r304.ds;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public record Transaction(String libelle) {
    public static Transaction of(String libelle) {
        return new Transaction(libelle);
    }
    public static List<Transaction> of(String... libelles) {
        return Arrays.stream(libelles).map(Transaction::of).collect(Collectors.toList());
    }
}
