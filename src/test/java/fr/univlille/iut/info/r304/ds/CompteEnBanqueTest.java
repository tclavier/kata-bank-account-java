package fr.univlille.iut.info.r304.ds;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CompteEnBanqueTest {
    @BeforeEach
    void nettoyerLesNomsDeComptes() {
        CompteEnBanque.nettoyer();
    }
    @Test
    void le_solde_doit_etre_de_0_a_la_creation() {
        CompteEnBanque compteEnBanqueDeBob = CompteEnBanque.creer("Bob");
        assertEquals(0.0, compteEnBanqueDeBob.solde());
    }

    @Test
    void un_depot_doit_incrementer_le_solde() {
        CompteEnBanque compteDeBob = CompteEnBanque.creer("Bob");
        compteDeBob.depot(12.2);
        assertEquals(12.2, compteDeBob.solde());
    }

    @Test
    void un_retrait_doit_decrementer_le_solde() {
        CompteEnBanque compteDeBob = CompteEnBanque.creer("Bob").with(12.2);
        compteDeBob.retrait(2.2);
        assertEquals(10.0, compteDeBob.solde());
    }

    @Test
    void il_est_impossible_de_faire_un_retrait_avec_un_solde_insuffisant() {
        CompteEnBanque compteDeBob = CompteEnBanque.creer("Bob");
        assertThrows(RuntimeException.class, () -> {
            compteDeBob.retrait(2.2);
        });
    }

    @Test
    void la_creation_doit_etre_enregistree_dans_l_historique() {
        CompteEnBanque compteDeBob = CompteEnBanque.creer("Bob");
        assertEquals(List.of(Transaction.of("Création")), compteDeBob.historique());
    }

    @Test
    void le_depot_doit_etre_enregistre_dans_l_historique() {
        CompteEnBanque compteDeBob = CompteEnBanque.creer("Bob");
        compteDeBob.depot(42);
        assertEquals(Transaction.of("Création", "Dépôt de 42.00 €"), compteDeBob.historique());
    }

    @Test
    void le_retrait_doit_etre_enregistre_dans_l_historique() {
        CompteEnBanque compteDeBob = CompteEnBanque.creer("Bob").with(50);
        compteDeBob.retrait(8);
        assertEquals(Transaction.of("Création", "Dépôt de 50.00 €", "Retrait de 8.00 €"), compteDeBob.historique());
    }

    @Test
    void il_n_est_pas_possible_de_creer_un_compte_avec_le_nom_d_un_compte_existant() {
        CompteEnBanque.creer("Bob");
        assertThrows(RuntimeException.class, () -> {
            CompteEnBanque.creer("Bob");
        });
    }
}
