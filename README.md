# Sujet

Nous allons chercher à tester une classe "compte en banque", sur laquelle il est possible de faire les opérations suivante :

- `static CompteEnBanque creer(String name)`
- ̀`void depot(Double montant)`
- `void retrait(Double montant)`
- `Double solde()`
- `List<Transaction> historique()`

Note: `Transaction` est une classe représentant chaque opération (ouverture, dépot, retrait, fermeture) effectuée sur un compte.
Inutile de tester cette classe ici, seules les méthodes de `CompteEnBanque` doivent être testées.

Rédigez et ordonnez les tests que vous feriez (au moins 5) dans une approche TDD.
Pour chaque test, donnez l’intention de code qu’il fait émerger.
